import sys, os
from shutil import rmtree
import json

class Constants:
    DOCKER_NAME = "bitbaker"
    DOCKER_IMG = "siana/mp1-openstlinux:v4.0"
    HOST_PATH_SOURCES = "sources/ostl-v3.0.0.json"
    PATH_REPO = "/repo"
    PATH_CACHE = "/cache"
    PATH_LAYER = f"{PATH_REPO}/layers"
    PATH_BUILD = f"{PATH_REPO}/build"
    PATH_SSH = "/home/builder/.ssh"
    PATH_SCRIPT = f"{PATH_REPO}/env.sh -b {PATH_BUILD} -s {PATH_SSH} -c {PATH_CACHE}"
    PATH_DEPLOY=f"{PATH_BUILD}/tmp-glibc/deploy"
    PATH_RELEASE=f"{PATH_REPO}/release"
    PATH_RELEASE_DOC=f"{PATH_RELEASE}/Documentation"
    CORE_LAYERS = f"\
        {PATH_LAYER}/openembedded-core/meta \
        {PATH_LAYER}/meta-openembedded/meta-oe \
        {PATH_LAYER}/meta-openembedded/meta-python \
        {PATH_LAYER}/meta-openembedded/meta-networking \
        {PATH_LAYER}/meta-openembedded/meta-multimedia \
        {PATH_LAYER}/meta-openembedded/meta-initramfs \
        {PATH_LAYER}/meta-openembedded/meta-filesystems \
        {PATH_LAYER}/meta-openembedded/meta-perl \
        {PATH_LAYER}/meta-st-stm32mp \
        "
    CUBEMX_LAYERS = f"{PATH_LAYER}/meta-st-stm32mp-addons "
    MENDER_LAYERS = f"{PATH_LAYER}/meta-mender/meta-mender-core \
                      {PATH_LAYER}/meta-mender-community/meta-mender-st-stm32mp "

class Bitbaker:
    @staticmethod
    def generateLayers(with_cubemx:bool, with_mender:bool) -> str:
        layers = Constants.CORE_LAYERS
        if with_cubemx:
            layers += Constants.CUBEMX_LAYERS
        if with_mender:
            layers += Constants.MENDER_LAYERS
        return layers

    @staticmethod
    def getSourcesCmd(with_cubemx:bool, with_mender:bool) -> str:
        modules = []
        with open(Constants.HOST_PATH_SOURCES, 'r') as j:
            sources = json.load(j)
            modules = sources['module_core']
            if with_cubemx:
                modules.append(sources['module_mx'])
            if with_mender:
                modules.append(sources['module_mender'])
                modules.append(sources['module_mender_community'])
        cmd = ""
        for sm in modules:
            if sm['branch'] != None:
                cmd += f"git submodule add -b {sm['branch']} {sm['url']} {sm['path']}"
            else:
                cmd += f"git submodule add {sm['url']} {sm['path']}"
            cmd += " && "
            if sm['rev'] != None:
                cmd += f"cd {Constants.PATH_REPO}/{sm['path']} && git checkout {sm['rev']} && cd {Constants.PATH_REPO}"
                cmd += " && "
        return cmd.strip(" && ")

    @staticmethod
    def configureLayersCmd(layers:str) -> str:
        return f"bitbake-layers add-layer {layers}"

    @staticmethod
    def configureBBCmd(distro:str, machine:str, with_mender:bool, secureRoot=False) -> str:
        path_conf = f"{Constants.PATH_BUILD}/conf/local.conf"
        tmp_conf = "tmp.conf"
        cmd = ""
        # Add mender configuration if it's in use
        if with_mender:
            cmd += Bitbaker.writeApend(path_conf, f"{Constants.PATH_LAYER}/meta-mender-community/templates/local.conf.append")
            cmd += " && "
            cmd += Bitbaker.writeApend(path_conf, f"{Constants.PATH_LAYER}/meta-mender-community/meta-mender-st-stm32mp/templates/local.conf.append")
            cmd += " && "
        
        # Add configurations
        Bitbaker.writeConfig(tmp_conf, "MACHINE", machine)
        Bitbaker.writeConfig(tmp_conf, "SSTATE_DIR", f"{Constants.PATH_CACHE}/sstate-cache")
        Bitbaker.writeConfig(tmp_conf, "DL_DIR", f"{Constants.PATH_CACHE}/downloads")
        Bitbaker.writeConfig(tmp_conf, "DISTRO", distro)
        Bitbaker.writeConfig(tmp_conf, f"ACCEPT_EULA_{machine}", "1")
        if secureRoot:
            Bitbaker.writeConfig(tmp_conf, "EXTRA_IMAGE_FEATURES", "")
        cmd += Bitbaker.writeApend(path_conf, f"{Constants.PATH_REPO}/{tmp_conf}")
        cmd += f" && rm {Constants.PATH_REPO}/{tmp_conf}"
        return cmd

    @staticmethod
    def dockerRunCmd(mounts:str, cmd:str, privileged=False) -> str:
        flag = Bitbaker.dockerIsPrivileged(privileged)
        return f"docker run --rm {flag} --name {Constants.DOCKER_NAME} {mounts} {Constants.DOCKER_IMG} bash -c \"{cmd}\""

    @staticmethod
    def runBBCmd(mounts:str, cmd:str, privileged=False) -> str:
        tmp = Bitbaker.dockerRunCmd(mounts, cmd=f"source {Constants.PATH_SCRIPT} && {cmd}", privileged=privileged)
        return tmp

    @staticmethod
    def dockerIt(mounts:str, privileged=False) -> str:
        flag = Bitbaker.dockerIsPrivileged(privileged)
        return f"docker run --rm {flag} {mounts} -it {Constants.DOCKER_IMG}"

    @staticmethod
    def dockerIsPrivileged(privileged:bool) -> str:
        flag = ""
        if privileged:
            flag = "--privileged"
        return flag

    @staticmethod
    def writeConfig(file_path:str, parameter:str, value:str) -> str:
        """
        Helper function to write OE parameter to a file.

        :param str file: File to write the parameter.
        :param str parameter: Parameter name.
        :param str value: Value for the parameter.
        """
        # to ensure weird behaivour of invoke passing quotes, write to a tmp file
        with open(file_path, 'a+') as f:
            f.write(f"{parameter} = \"{value}\"\n")

    @staticmethod
    def writeApend(path_original:str, path_append:str) -> str:
        """
        Helper function to append contents of a file to another

        :param str file: Path of the file to append to.
        :param str file: Path of the file with content to be appended..
        """
        return f"cat {path_append} >> {path_original}"

    @staticmethod
    def getSWU(update:str, machine:str) -> str:
        return f"\
            cd {Constants.PATH_DEPLOY}/images/{machine} && \
            cp {update}-update-{machine}.swu {Constants.PATH_REPO}"

    @staticmethod
    def getArtifact(update:str, packageManager:str='ipk', arch:str='cortexa7t2hf-neon-vfpv4') -> str:
        updatesPath = 'meta-mpcam/recipes-mpcam/updates'
        package = f"{update}_*.{packageManager}"

        cmd = f"\
            cd {Constants.PATH_DEPLOY}/{packageManager}/{arch} && \
            mv {package} {update} && \
            cp {update} {Constants.PATH_LAYER}/{updatesPath}/{update+'-update'}"
        return cmd
    
    @staticmethod
    def getVersion(path:str) -> str:
        """
        Gets version from "RELEASE" parameter from OE file.

        :param str path: path to OE's config file with RELEASE parameter.
        :return str: Version string.
        """
        with open(path, 'r') as f:
            for line in f:
                if "RELEASE = " in line:
                    return line.replace("RELEASE = ", "").replace("\"", "").strip()
    
    @staticmethod
    def getReleaseName(client:str, projectName:str, machine:str, type:str, version:str) -> str:
        """
        Generates release name following SIANA naming convention.

        :param str client: Project's client.
        :param str projectName: Project's name, normally distro name.
        :param str machine: OE machine.
        :param str distro: OE Distro.
        :param str type: Type of release, normally SDC, eMMC or SDK.
        :param str version: Version number, normally obtained with getVersion().
        :return str: Release name string.
        """
        if projectName == None:
            return f"{client}-{machine}-{type}-v{version}"
        return f"{client}-{projectName}-{machine}-{type}-v{version}"

    def makeDocs(image:str, machine:str) -> str:
        """
        Generates commands to copy Documentation for the releases.
        Obtain list of licenses and packages in the image and places them into
        PATH_RELEASE_DOCS folder.

        :param str machine: OE machine.
        :param str distro: OE Distro.
        :return str: Commands to populate documentation of a release.
        """
        return f"\
            mkdir -p {Constants.PATH_RELEASE_DOC} && \
            cd {Constants.PATH_DEPLOY} && \
            tree licenses > {Constants.PATH_RELEASE_DOC}/licenses.txt && \
            cd images/{machine} && \
            cp {image}-{machine}.manifest {Constants.PATH_RELEASE_DOC}/packages.manifest && \
            cp {Constants.PATH_REPO}/*.md {Constants.PATH_RELEASE_DOC}"

    @staticmethod
    def zipRelease(name:str) -> str:
        """
        Compresses the temporal release folder, names the proper release and
        deletes the temporal folder.

        :param str name: File name for the zip package.
        :return str: Commands to zip release folder.
        """
        return f"\
            cd {Constants.PATH_RELEASE} && \
            zip -r {Constants.PATH_REPO}/{name}.zip . && \
            rm -rf {Constants.PATH_RELEASE}"

    @staticmethod
    def makeSDCard(client:str, projectName:str, image:str, machine:str, version:str) -> str:
        """
        Packages SDCard release using flash layout script.

        :param str client: Project's client.
        :param str projectName: Project's name, normally distro name.
        :param str image: OE image recipe.
        :param str machine: OE machine.
        :param str distro: OE Distro.
        :param str version: Version of the release.
        :return str: Commands generate SDCard release.
        """
        name = Bitbaker.getReleaseName(client, projectName, machine, "SDC", version)
        return f"{Bitbaker.makeDocs(image, machine)} && \
            ./scripts/create_sdcard_from_flashlayout.sh flashlayout_{image}/trusted/*sdcard*.tsv && \
            mv *{machine}*.raw {Constants.PATH_RELEASE}/{name}.img && \
            {Bitbaker.zipRelease(name)}"

    @staticmethod
    def makeEMMC(client:str, projectName:str, image:str, machine:str, version:str) -> str:
        """
        Packages eMMC release using flash layout and STM32CubeProgrammer.

        :param str client: Project's client.
        :param str projectName: Project's name, normally distro name.
        :param str image: OE image recipe.
        :param str machine: OE machine.
        :param str distro: OE Distro.
        :param str version: Version of the release.
        :return str: Commands generate eMMC release.
        """
        name = Bitbaker.getReleaseName(client, projectName, machine, "eMMC", version)
        return f"{Bitbaker.makeDocs(image, machine)} && \
            cp flashlayout_{image}/trusted/*emmc*.tsv {Constants.PATH_RELEASE}/{name}.tsv && \
            cp -r fip {Constants.PATH_RELEASE} && \
            cp -r arm-trusted-firmware {Constants.PATH_RELEASE} && \
            cp -r u-boot {Constants.PATH_RELEASE} && \
            cp *{machine}.ext4 {Constants.PATH_RELEASE} && \
            {Bitbaker.zipRelease(name)}"

    @staticmethod
    def makeSDK(client:str, projectName:str, image:str, machine:str, version:str) -> str:
        """
        Packages SDK release.

        :param str client: Project's client.
        :param str projectName: Project's name, normally distro name.
        :param str image: OE image recipe.
        :param str machine: OE machine.
        :param str distro: OE Distro.
        :param str version: Version of the release.
        :return str: Commands generate SDK release.
        """
        name = Bitbaker.getReleaseName(client, projectName, machine, "SDK", version)
        return f"\
            mkdir -p {Constants.PATH_RELEASE} && \
            mv {Constants.PATH_DEPLOY}/sdk/* {Constants.PATH_RELEASE} && \
            {Bitbaker.zipRelease(name)}"
    
    @staticmethod
    def findPackage(package:str) -> str:
        """
        Finds a package by name.

        :param str package: Package name.
        :return str: Command to search.
        """
        return  f"find {Constants.PATH_DEPLOY} -name *{package}*"
    
    @staticmethod
    def packageInfo(path:str, keep=False) -> str:
        """
        Gets information from a package.

        :param str path: Full (docker) path of the package.
        :param bool keep: if enabled, package contents are not deleted.
        :return str: Commands to show package information.
        """
        tmp = os.path.splitext(os.path.basename(path))[0]
        cmd = f"mkdir {tmp} && \
            cd {tmp} && \
            ar -x {path} && \
            tar -xf control.* && \
            cat control && \
            echo 'Contents: ' && \
            tar -tvf data.* && \
            cd .. "
        if not keep:
            cmd += f"&& rm -rf {tmp}"
        return cmd
