#!/bin/bash

# EMMC
BLK=mmcblk1
MMC=/dev/$BLK

# Files
FSBL=arm-trusted-firmware/tf-a-stm32mp157c-mpcam-mx-trusted.stm32
SSBL=bootloader/u-boot-stm32mp157c-mpcam-mx-trusted.stm32
BOOTFS=st-image-bootfs-mpcam-mpcam.ext4
VENDORFS=st-image-vendorfs-mpcam-mpcam.ext4
ROOTFS=mpcam-image-demo-mpcam.ext4
ROOTFS_UUID=491f6117-415d-4f53-88c9-6e0de54deac6

# Clear partitions
sgdisk -og -a 1 $MMC
# Create partitions
# 4095, 2 MiB
sgdisk -a 1 -n 1:34:4129 -c 1:ssbl -t 1:8301 $MMC
# 131071, 64 MiB with boot flag
sgdisk -a 1 -n 2:4130:135201 -c 2:boot -t 2:8300 -A 2:set:2 $MMC
# 32767, 16 MiB
sgdisk -a 1 -n 3:135202:167969 -c 3:vendorfs -t 3:8300 $MMC
# rest of the disk with specific UUID
sgdisk -a 1 -n 4:167970: -c 4:rootfs -t 4:8300 -u 4:$ROOTFS_UUID $MMC
# Show partitions
sgdisk -p $MMC

# Enable write to boot partitions on MMC
echo 0 > /sys/class/block/${BLK}boot0/force_ro
echo 0 > /sys/class/block/${BLK}boot1/force_ro

# Write FSBL: tf-a
dd if=$FSBL of=${MMC}boot0
dd if=$FSBL of=${MMC}boot1
dd if=$SSBL of=${MMC}p1
dd if=$BOOTFS of=${MMC}p2
dd if=$VENDORFS of=${MMC}p3
dd if=$ROOTFS of=${MMC}p4