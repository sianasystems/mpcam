inherit setuptools3

SUMMARY = "A series of convenience functions to make basic image processing functions such as translation, rotation, resizing, skeletonization, and displaying Matplotlib images easier with OpenCV and both Python 2.7 and Python 3."
SECTION = "apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=1360854e0617129c509b839f6a51874f"

SRC_URI = "git://github.com/siana-systems/imutils.git;protocol=https;branch=master \
           file://0001-change-setup-function-from-distutils-to-setuptools.patch \
           "

SRCREV = "0928c5baea2f58e1c48dc8e8434f77595953a9ea"
S = "${WORKDIR}/git"

RDEPENDS:{PN} = " opencv"
