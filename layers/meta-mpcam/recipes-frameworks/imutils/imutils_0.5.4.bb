inherit pypi setuptools3

SUMMARY = "A series of convenience functions to make basic image processing functions such as translation, rotation, resizing, skeletonization, and displaying Matplotlib images easier with OpenCV and both Python 2.7 and Python 3."
SECTION = "apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://PKG-INFO;md5=06ee56a47d920722daa6cc4e7139fe63"

SRC_URI[sha256sum] = "03827a9fca8b5c540305c0844a62591cf35a0caec199cb0f2f0a4a0fb15d8f24"

PYPI_PACKAGE = "imutils"
CLEANBROKEN = "1"

RDEPENDS:{PN} = " opencv"
