SUMMARY = "A Python library to create SVG drawings"
HOMEPAGE = " http://github.com/mozman/svgwrite.git"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.txt.rst;md5=3e14f2d1a8674ddcbbd8b51762250049"

inherit pypi setuptools3

PYPI_PACKAGE = "svgwrite"

BBCLASSEXTEND = "native nativesdk"
