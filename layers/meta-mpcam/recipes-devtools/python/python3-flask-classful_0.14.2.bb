SUMMARY = "Class based views for Flask"
HOMEPAGE = " https://github.com/teracyhq/flask-classful"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://README.rst;md5=c4ec05372cde615daff0dfc598de2069"

SRC_URI[sha256sum] = "ae0cabd3f69e1e16db3bf31f50862d3a5577244bfa0ace2b4612d10436e4bff7"

inherit pypi setuptools3

PYPI_PACKAGE = "Flask-Classful"

RDEPENDS_${PN} = "\
    ${PYTHON_PN}-flask \
    "

BBCLASSEXTEND = "native nativesdk"
