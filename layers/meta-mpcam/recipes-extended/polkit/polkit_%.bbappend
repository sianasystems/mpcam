FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI =+ "file://60-mpcam.rules"

do_install:append() {
    install ${WORKDIR}/60-mpcam.rules ${D}${sysconfdir_native}/polkit-1/rules.d/
}

FILES_${PN}:append = " ${sysconfdir_native}/polkit-1/rules.d/"