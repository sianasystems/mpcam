# from dev manual:
# Your configuration file needs to set the following variables:
#
#      DISTRO_NAME [required]
#      DISTRO_VERSION [required]
#      DISTRO_FEATURES [required if creating from scratch]
#      DISTRO_EXTRA_RDEPENDS [optional]
#      DISTRO_EXTRA_RRECOMMENDS [optional]
#      TCLIBC [required if creating from scratch]

require conf/distro/include/no-static-libs.inc
require conf/distro/include/yocto-uninative.inc
INHERIT += "uninative"

# Init Release version MM.mm
RELEASE = "03.02"

DISTRO_NAME = "mpcam"
DISTRO_VERSION = "${RELEASE}"
# Don't include the DATE variable in the sstate package signatures
DISTRO_VERSION[vardepsexclude] = "DATE"
DISTRO_CODENAME = "kirkstone"

# Warning: SDK_VENDOR does not contains a valid OS/ARCH name like : linux, arm
SDK_VENDOR = "-${DISTRO_NAME}_sdk"
SDK_VERSION := "${@'${DISTRO_VERSION}'.replace('${DATE}','')}"
# Don't include the DATE variable in the sstate package signatures
SDK_VERSION[vardepsexclude] = "DATE"

MAINTAINER = "Sebastian Sepulveda <sebasitan@siana-systems.com>"

# Warning: TARGET_VENDOR does not contains a valid OS/ARCH name like : linux, arm
TARGET_VENDOR = "-${DISTRO_NAME}"

# Append distro name to each image name
#IMAGE_BASENAME += "-${DISTRO_NAME}"

# Add image name for generated SDK and set default SDK install folder
SDK_NAME = "${IMAGE_LINK_NAME}-${SDK_ARCH}"
SDKPATH = "/opt/st/${MACHINE}/${SDK_VERSION}"
# Don't include the DATE variable in the sstate package signatures
SDKPATH[vardepsexclude] = "DATE"

LOCALCONF_VERSION = "2"
LAYER_CONF_VERSION ?= "7"

IMAGE_LINGUAS = "en-us"

# =========================================================================
# DISTRO features
# =========================================================================
DISTRO_FEATURES  = "alsa"
DISTRO_FEATURES += "argp"
#DISTRO_FEATURES += "ext2"
DISTRO_FEATURES += "ext4"
DISTRO_FEATURES += "largefile"
DISTRO_FEATURES += "ipv4"
DISTRO_FEATURES += "ipv6"
#DISTRO_FEATURES += "multiarch"
DISTRO_FEATURES += "wifi"
#DISTRO_FEATURES += "nfs"
DISTRO_FEATURES += "usbgadget"
DISTRO_FEATURES += "usbhost"
DISTRO_FEATURES += "xattr"
DISTRO_FEATURES += "zeroconf"
DISTRO_FEATURES += "bluetooth"
DISTRO_FEATURES += "bluez5"
DISTRO_FEATURES += "${DISTRO_FEATURES_LIBC}"

# add support of optee
#DISTRO_FEATURES += " optee "

# add support of systemd
DISTRO_FEATURES += " systemd polkit "

# add support of efi
DISTRO_FEATURES += " efi "

# add support of InitRD installation package
#DISTRO_FEATURES += " initrd "

# add support of autoresize through InitRD
#DISTRO_FEATURES += " autoresize "

# add support of tpm2
#DISTRO_FEATURES += " tpm2 "

# Disabling pulseaudio
#DISTRO_FEATURES_BACKFILL_CONSIDERED += "pulseaudio"
DISTRO_FEATURES += "pulseaudio"

# Disabling sysvinit
DISTRO_FEATURES_BACKFILL_CONSIDERED += "sysvinit"

VIRTUAL-RUNTIME_init_manager = "systemd"
VIRTUAL-RUNTIME_initscripts = "systemd-compat-units"

# add support of python2
I_SWEAR_TO_MIGRATE_TO_PYTHON3 = "yes"

# =========================================================================
# Preferred version
# =========================================================================

# v4l-utils
#PREFERRED_VERSION_v4l-utils ?= "1.6.2"

# =========================================================================
# IMAGE addons
# =========================================================================
LICENSE_FLAGS_ACCEPTED += " non-commercial commercial"

# X11 addons
DISTRO_EXTRA_RDEPENDS += " ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'xf86-video-modesetting', '', d)} "


# =========================================================================
# MPCAM specifics
# =========================================================================
# Needed by vtracker
#PACKAGECONFIG:append_pn-opencv = " gtk"
