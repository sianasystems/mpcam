FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# Disable kernel debug features
SRC_URI += " file://disable-kernel-debug.cfg "
KERNEL_CONFIG_FRAGMENTS += "${WORKDIR}/disable-kernel-debug.cfg"

#SRC_URI += "file://enable_earlyprintk.cfg"
SRC_URI += "file://enable_usb_wifi.cfg"

#KERNEL_CONFIG_FRAGMENTS += "${WORKDIR}/enable_earlyprintk.cfg"
KERNEL_CONFIG_FRAGMENTS += "${WORKDIR}/enable_usb_wifi.cfg"

# enable EXT with extended attributes xattr
SRC_URI += "file://enable_ext.cfg"
KERNEL_CONFIG_FRAGMENTS += "${WORKDIR}/enable_ext.cfg"

SRC_URI += " file://enable_kprobes.cfg "
KERNEL_CONFIG_FRAGMENTS += "${WORKDIR}/enable_kprobes.cfg"

# Driver optimization
SRC_URI += " file://optimize-drivers.cfg "
KERNEL_CONFIG_FRAGMENTS += "${WORKDIR}/optimize-drivers.cfg"