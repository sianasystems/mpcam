SUMMARY = "MPCam vtracker demo"
LICENSE = "CLOSED"

APP = "mpcam-vtracker"
SRC_URI = "file://${APP}.service"

inherit systemd

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "mpcam-vtracker.service"

USER = "mpcam"

RDEPENDS:${PN} = "\
    bash opencv python3-core python3-pygobject \
    python3-numpy python3-pandas python3-pillow \
    python3-pyparsing python3-dateutil python3-pytz \
    python3-pyyaml python3-redis python3-six \
    python3-webcolors python3-setuptools \
    python3-flask python3-flask-socketio python3-socketio \
    python3-pystemd python3-humanize \
    python3-beautifulsoup4 python3-flask-classful libusb1 \
    python3-pympcam python3-netifaces avahi-utils lsb-release \
    libedgetpu mpcam-users \
    "

do_install() {
    # create folders
    install -d ${D}${systemd_unitdir}/system

    # systemd
    install -m 0644 ${WORKDIR}/${APP}.service ${D}${systemd_unitdir}/system
}

FILES:${PN} += " ${systemd_unitdir} /home/${USER}"

INSANE_SKIP:${PN} += "already-stripped ldflags"