SUMMARY = "MPCam self test script"
LICENSE = "MIT"

SRC_URI = "file://mpcam-self-test.py \
           file://LICENSE"
APP_NAME = "mpcam-self-test"
LIC_FILES_CHKSUM = "file://LICENSE;md5=a9e46f382eb7dc3fc3e33871308ce34f"

S = "${WORKDIR}"

RDEPENDS:${PN} = "python3 python3-core"

do_install() {
    # create folders
    install -d ${D}${bindir}

    # binaries
    install ${S}/${APP_NAME}.py ${D}${bindir}/${APP_NAME}
}

FILES_${PN} += "${bindir}"