#!/usr/bin/python3
##
# @file       mpcam-self-test.py
# @author     SIANA Systems
# @date       04/23/2021
# @copyright  The MIT License (MIT)
# @brief      MPCam self test.
#
# -----------------------------------------------------------------------------
# The MIT License (MIT)
#
# Copyright (c) 2021 SIANA Systems
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# -----------------------------------------------------------------------------
__author__ = "SIANA Systems"
__license__ = "MIT"
__version__ = "ALPHA"
__email__ = "support@siana-systems.com"
# -----------------------------------------------------------------------------

from pympcam.irLed import IrLed
from pympcam.userButtons import UserButtons
from pympcam.userLed import UserLed
from pympcam.coralManager import CoralManager
import subprocess
from time import sleep
from os import path

class MPCamSelfTest:
    OK = "PASS"
    NOK = "FAIL"

    def __init__(self):
        self.report = ""

    def run(self):
        print("Starting interactive tests")
        self.irLed()
        self.userLeds()
        self.userButtons()

        print("\nStarting automated tests", flush=True)
        self.coralDetect()
        self.checkEmmc()
        self.microphone()

        # Show results
        print(f"\nTest results for SN {self.getCpuSerialNumber()}:\n{self.report}")

    def irLed(self) -> None:
        ir = IrLed()
        ir.turnOn()
        resp = self.NOK
        if self.yn("Are IR LEDs on ?"):
            resp = self.OK
        self.addToReport("IR LED", resp)
        ir.turnOff()
    
    def userLeds(self) -> None:
        led = UserLed()
        led.turnOff()
        for l in [["LED1", "blue"], ["LED2", "red"]]:
            led.turnOn(l[0])
            resp = self.NOK
            if self.yn(f"Is {l[0]} ({l[1]}) on ?"):
                resp = self.OK
            led.turnOff(l[0])
            self.addToReport(f"User {l[0]} ({l[1]})", resp)
    
    def userButtons(self, timeout_sec=10) -> None:
        btn = UserButtons()
        for sw in [["SW1", btn.SW1], ["SW2", btn.SW2]]:
            print(f"Press {sw[0]} during the next {timeout_sec} seconds", flush=True)
            resp = self.NOK
            if btn.pollState(sw[1], timeout_sec):
                resp = self.OK
            self.addToReport(f"Button {sw[0]}", resp)
    
    def coralDetect(self, timeout_sec=1) -> None:
        coral = CoralManager()
        coral.turnOn()
        sleep(timeout_sec)
        ret = subprocess.run(["lsusb"], capture_output=True)
        resp = self.NOK
        if "1a6e:089a" in ret.stdout.decode():
            resp = self.OK
        coral.turnOff()
        self.addToReport(f"Coral detected", resp)

    def checkEmmc(self) -> None:
        p = '/sys/class/block/mmcblk1/size'
        resp = self.NOK
        size = 0
        if path.exists(p):
            with open(p, 'r') as f:
                size = (int(f.read()) * 512)/1024000000
                resp = self.OK
        self.addToReport(f"eMMC detection: {size:.2f} GiB", resp)

    def microphone(self, time_msec=1000, expected_size_kb=1.9) -> None:
        p = 'test.raw'
        subprocess.run(['rm', p])
        cmd = ['arecord', '-f', 'S16_LE', '-r', '16000', '-s', f'{time_msec}', f'{p}']
        ret = subprocess.run(cmd, capture_output=True)
        resp = self.NOK
        size = 0
        if path.exists(p):
            size = int(path.getsize(p)) / 1024
            if size > expected_size_kb:
                resp = self.OK
        self.addToReport(f"Microphone recording: {size:.2f} KiB", resp)

    def getCpuSerialNumber(self) -> None:
        cmd = ['cat', '/proc/cpuinfo']
        ret = subprocess.run(cmd, capture_output=True)
        for line in ret.stdout.decode().split('\n'):
            if line.startswith("Serial"):
                return line.split(':')[1]
        
    def addToReport(self, title:str, result:str) -> None:
        self.report += f"[{result}] {title}\n"

    @staticmethod
    def yn(question:str) -> bool:
        validResponse = False
        while not validResponse:
            reply = input(question+' [y/n]: ').lower().strip()
            if len(reply) == 1:
                if reply[0] == 'y':
                    return True
                if reply[0] == 'n':
                    return False

if __name__ == "__main__":
    mpctests = MPCamSelfTest()
    mpctests.run()
