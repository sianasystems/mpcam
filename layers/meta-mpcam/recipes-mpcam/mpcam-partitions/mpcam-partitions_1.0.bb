SUMMARY = "AVID data Image"
LICENSE = "CLOSED"

APP = "mpcam-partitions"
SRC_URI = "file://${APP} \
           file://${APP}.service"

inherit systemd
SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "${APP}.service"

RDEPENDS:${PN} = "e2fsprogs-resize2fs bash"

do_install:append() {
    install -d ${D}${systemd_unitdir}/system
    install -d ${D}${bindir}

    install -m 0644 ${WORKDIR}/${APP}.service ${D}${systemd_unitdir}/system
    install ${WORKDIR}/${APP} ${D}${bindir}
}

FILES_${PN} += "${systemd_unitdir} ${bindir}"