SUMMARY = "MPCam PyMPCam Python module"
LICENSE = "MIT"

TAG = "v${PV}"
SRC_URI = "git://github.com/siana-systems/pympcam.git;protocol=https;branch=master"
LIC_FILES_CHKSUM = "file://LICENSE;md5=ebc9b482e97d906752fad7ecbae0ca32"

SRCREV = "07fe1f3595fbf88d4097e0335b25a27c4b6d98aa"

inherit setuptools3

S = "${WORKDIR}/git"

RDEPENDS:${PN} += "python3-periphery"
DEPENDS += "python3-periphery"
