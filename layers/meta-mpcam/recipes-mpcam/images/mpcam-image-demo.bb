SUMMARY = "MPCAM image demo."
LICENSE = "Proprietary"

include mpcam-core.inc

inherit core-image

CORE_IMAGE_EXTRA_INSTALL += "\
    mpcam-http \
    mpcam-self-test \
    mpcam-partitions \
    "
# MPCam demos
CORE_IMAGE_EXTRA_INSTALL += "\
    mpcam-stura-vtracker \
    mpcam-demo-posenet \
    mpcam-demo-detector \
    "
