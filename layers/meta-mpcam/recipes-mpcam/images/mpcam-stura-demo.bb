SUMMARY = "MPCAM image demo."
LICENSE = "Proprietary"

include mpcam-core.inc

inherit core-image

CORE_IMAGE_EXTRA_INSTALL += "\
    mpcam-self-test \
    mpcam-partitions \
    mpcam-stura-vtracker \
    mpcam-vtracker \
    "
