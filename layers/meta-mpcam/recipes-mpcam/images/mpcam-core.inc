IMAGE_FEATURES += "\
    hwcodecs \
    package-management \
    ssh-server-dropbear \
    tools-profile \
    eclipse-debug \
    "
# Base apps
CORE_IMAGE_EXTRA_INSTALL += "\
    libjpeg-turbo \
    htop \
    nano \
    minicom \
    gptfdisk \
    tar \
    lsb-release \
    coreutils \
    e2fsprogs \
    e2fsprogs-resize2fs \
    python3-pip \
    "

# MPCam general dependencies
CORE_IMAGE_EXTRA_INSTALL += "\
    networkmanager \
    python \
    python3 \
    python3-numpy \
    opencv \
    libedgetpu \
    tensorflow-lite \
    v4l-utils \
    i2c-tools \
    libgpiod \
    alsa-utils \
    siana-imutils \
    python3-pympcam \
    mpcam-coral \
    linux-firmware \
    python3-pycoral \
    "

# GStreammer
CORE_IMAGE_EXTRA_INSTALL += "\
    gstreamer1.0-plugins-base-meta \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good-meta \
    gstreamer1.0-plugins-bad-meta \
    gstreamer1.0-plugins-ugly-meta \
    gstreamer1.0-libav \
    gstreamer1.0-rtsp-server-meta \
    "
# USB/WiFi support
CORE_IMAGE_EXTRA_INSTALL += "\
    usb-modeswitch \
    8188eu \
    "

# SWUpdate support
CORE_IMAGE_EXTRA_INSTALL += " \
    swupdate \
    swupdate-usb \
    swupdate-progress \
    e2fsprogs-resize2fs \
    u-boot-fw-utils \
    "

# Redis support
CORE_IMAGE_EXTRA_INSTALL += "\
    redis \
    python3-redis \
    "