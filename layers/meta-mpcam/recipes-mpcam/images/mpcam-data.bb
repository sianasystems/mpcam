SUMMARY = "AVID data Image"
LICENSE = "MIT"

include recipes-st/images/st-image-partitions.inc

# Match partition size with FS size
pkg_postinst_ontarget_${PN}() {
    resize2fs /dev/mmcblk0p10
    resize2fs /dev/mmcblk0p11
}