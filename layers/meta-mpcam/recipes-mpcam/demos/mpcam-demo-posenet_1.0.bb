SUMMARY = "MPCam Posenet demo"
LICENSE = "CLOSED"

SRC_URI = "git://git@github.com/siana-systems/mpcam-demo-posenet.git;protocol=ssh;branch=master"
SRCREV = "7d4ff3732d7ea338a5b598b0ea5045834db2321d"
APP_NAME = "mpcam.posenet.demo"
S = "${WORKDIR}/git"

inherit gitpkgv

DEPENDS = "mpcam-http libusb"

do_install:append() {
    # create folders
    install -d ${D}/usr/local
    install -d ${D}/usr/local/mpcam-demos
    install -d ${D}/usr/local/mpcam-demos/mpcam-demo-posenet
    # demo data
    cp -r ${S}/* ${D}/usr/local/mpcam-demos/mpcam-demo-posenet
}

FILES:${PN} += "/usr/local/*"
INSANE_SKIP:${PN} += "already-stripped ldflags"