SUMMARY = "MPCam Detector demo"
LICENSE = "CLOSED"

SRC_URI = "git://git@github.com/siana-systems/mpcam-demo-detector.git;protocol=ssh;branch=master"
SRCREV = "5e7902b5a057bbf528ff448b5975373b716a95e9"
APP_NAME = "mpcam.detector.demo"
S = "${WORKDIR}/git"

inherit gitpkgv

DEPENDS = "mpcam-http libusb"

do_install:append() {
    # create folders
    install -d ${D}/usr/local
    install -d ${D}/usr/local/mpcam-demos
    install -d ${D}/usr/local/mpcam-demos/mpcam-demo-detector
    # demo data
    cp -r ${S}/* ${D}/usr/local/mpcam-demos/mpcam-demo-detector
}

pkg_postinst_ontarget_${PN} () {
    chown -R ${USER}:${USER} ${LOCAL}/mpcam-demos
}

FILES:${PN} += "/usr/local/*"
INSANE_SKIP:${PN} += "already-stripped ldflags"