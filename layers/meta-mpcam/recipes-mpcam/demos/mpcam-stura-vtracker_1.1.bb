SUMMARY = "MPCam Stura vTracker demo"
LICENSE = "CLOSED"

SRC_URI = "gitsm://git@bitbucket.org/sianasystems/mpcam.stura.vtracker.git;protocol=ssh;branch=master \
           file://0001-Enable-redis-storing-publishing-and-suscribing-mecha.patch \
           "
SRCREV = "d4297aea3da0070a13cd439ecb0fe18b0ea2da85"
APP_NAME = "mpcam.stura.vtracker"
S = "${WORKDIR}/git"

inherit gitpkgv

DEPENDS = "libusb"

do_install:append() {
    # create folders
    install -d ${D}/usr/local
    install -d ${D}/usr/local/mpcam-demos
    install -d ${D}/usr/local/mpcam-demos/mpcam.stura.vtracker
    # demo data
    cp -r ${S}/* ${D}/usr/local/mpcam-demos/mpcam.stura.vtracker
}

pkg_postinst_ontarget_${PN} () {
    chown -R ${USER}:${USER} ${LOCAL}/mpcam-demos
}

FILES:${PN} += "/usr/local/*"
INSANE_SKIP:${PN} += "already-stripped ldflags"