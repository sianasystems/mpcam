# https://wiki.yoctoproject.org/wiki/images/e/e6/Custom_Users_Groups_in_Yocto1.1.pdf
SUMMARY = "MPCam user and group"
DESCRIPTION = "MPCam user and group definition"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

USER = "mpcam"
# Obtained with `openssl passwd -crypt -salt pass mpcam`
PSWD = "pafBNnfDSnbCk"
GROUPS = "audio,dialout,video,adm,plugdev,sudo,shutdown"
xGROUPS = "polkitd,systemd-journal"

inherit useradd

USERADD_PACKAGES = "${PN}"
USERADD_PARAM:${PN} = "-d /home/${USER} -U -G ${GROUPS} -m -p ${PSWD} -r ${USER}"

inherit systemd
SRC_URI = "file://mpcam-permissions.service \
           file://mpcam-permissions \
           file://group-sudo \
           file://env.sh"
SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "mpcam-permissions.service"

inherit python3-dir
PY_USER_SITE_PACKAGES = "/home/${USER}/.local/lib/${PYTHON_DIR}/site-packages"

do_install () {
    install -d ${D}/data
    install -d ${D}/sdcard

    chown -R ${USER}:${USER} ${D}/data
    chown -R ${USER}:${USER} ${D}/sdcard

    # Permission for GPIO
    install -d ${D}${systemd_unitdir}/system
    install -d ${D}${bindir}
    install -m 0644 ${WORKDIR}/mpcam-permissions.service ${D}${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/mpcam-permissions ${D}${bindir}/
    chmod +x ${D}${bindir}/mpcam-permissions

    # Enable sudo group in sudoers
    install -d ${D}${sysconfdir}/sudoers.d
    install -m 0440 ${WORKDIR}/group-sudo ${D}${sysconfdir}/sudoers.d/group-sudo

    # Set Env variables
    install -d ${D}/${sysconfdir}/profile.d
    install -m 0755 ${WORKDIR}/env.sh ${D}/${sysconfdir}/profile.d/

    # Create pip site-package per user
    install -d ${D}/${PY_USER_SITE_PACKAGES}
    chown -R ${USER}:${USER} ${D}/${PY_USER_SITE_PACKAGES}
}

RDEPENDS:${PN} = "libcap-bin sudo bash"
pkg_postinst_ontarget:${PN} () {
    usermod -aG ${xGROUPS} ${USER}
    setcap CAP_NET_BIND_SERVICE=+eip $D${bindir}/python3.10
}

FILES:${PN} = "/home/${USER} /data /sdcard ${systemd_unitdir}/system ${bindir} ${sysconfdir} "
