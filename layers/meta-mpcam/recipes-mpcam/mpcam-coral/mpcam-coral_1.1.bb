SUMMARY = "MPCam Coral manager"
LICENSE = "MIT"

SRC_URI = "file://${BPN} \
           file://LICENSE"
LIC_FILES_CHKSUM = "file://LICENSE;md5=ebc9b482e97d906752fad7ecbae0ca32"
S = "${WORKDIR}"

RDEPENDS:${PN} = "python3-core python3-pympcam"

do_install() {
    install -d ${D}${bindir}/
    install -m 0755 ${S}/${PN} ${D}${bindir}/
}

FILES_${PN} = "${bindir}"