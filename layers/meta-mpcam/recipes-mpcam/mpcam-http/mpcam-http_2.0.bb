SUMMARY = "MPCam HTTP demo"
LICENSE = "CLOSED"

SRC_URI = "gitsm://git@bitbucket.org/sianasystems/mpcam.http.git;protocol=ssh;branch=develop"

SRCREV = "a979b6b99361b603368439ddf236a8a030ea34dd"
APP_NAME = "mpcam.http"

inherit systemd
inherit setuptools3

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "mpcam-http.service"

S = "${WORKDIR}/git"
LOCAL = "${prefix}/local"
USER = "mpcam"

RDEPENDS:${PN} = "\
    bash opencv python3-core python3-pygobject \
    python3-numpy python3-pandas python3-pillow \
    python3-pyparsing python3-dateutil python3-pytz \
    python3-pyyaml python3-redis python3-six \
    python3-webcolors python3-setuptools \
    python3-flask python3-flask-socketio python3-socketio \
    python3-pystemd python3-humanize \
    python3-beautifulsoup4 python3-flask-classful libusb1 \
    python3-pympcam python3-netifaces avahi-utils lsb-release \
    libedgetpu mpcam-users \
    "

do_install:append() {
    # create folders
    install -d ${D}${bindir}
    install -d ${D}${systemd_unitdir}/system
    
    # binaries    
    install ${S}/mpcam.http ${D}${bindir}
    
    #lnr ${D}${LOCAL}/mpcam-demos/object-detection/objects ${D}/home/${USER}/mpcam_root_folder/object_detection_images
    
    # systemd
    install -m 0644 ${S}/systemd/mpcam-http.service ${D}${systemd_unitdir}/system
}

pkg_postinst_ontarget_${PN} () {
    chown -R ${USER}:${USER} ${LOCAL}/mpcam-demos
}

FILES:${PN} += "${bindir} ${systemd_unitdir} /home/${USER}"

INSANE_SKIP:${PN} += "already-stripped ldflags"