DESCRIPTION = "SWU package for MPCam Stura vtracker demo app"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit swupdate

SRC_URI = "\
    file://sw-description \
    file://mpcam-stura-vtracker.sh \
    file://mpcam-stura-vtracker \
"
