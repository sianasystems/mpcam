#!/bin/sh

APP=mpcam-demo-detector
TAG="POST_INSTALL :"

echo "${TAG} Updating ${APP}..."
sudo opkg install /tmp/$APP --force-reinstall --force-overwrite
echo "${TAG} ${APP} update done!"
echo "${TAG} Cleaning..."
rm -rf /tmp/$APP*
rm -rf /tmp/sw-description
rm -rf /tmp/scripts

# successful update flag
sudo touch /etc/swupdate-update-ok
sudo systemctl restart swupdate
sudo systemctl restart swupdate-progress