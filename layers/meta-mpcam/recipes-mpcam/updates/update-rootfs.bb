DESCRIPTION = "SWU package for rootfs"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit swupdate

SRC_URI = "\
    file://sw-description \
    file://postupdate.sh \
"

PREFERRED_PROVIDER_u-boot-fw-utils = "libubootenv"

IMG = "mpcam-image-demo"

# images to build before building swupdate image
IMAGE_DEPENDS = "${IMG}"

# images and files that will be included in the .swu image
SWUPDATE_IMAGES = "${IMG}"

SWUPDATE_IMAGES_FSTYPES[mpcam-image-demo] = ".ext4"