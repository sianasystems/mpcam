#!/usr/bin/env python
##
# @file       tasks.py
# @author     SIANA Systems
# @date       12/2020
# @copyright  The MIT License (MIT)
# @brief      Python Invoke tasks script for the MPCam bring-up. 
#
# The script user-manual can be found on SIANA Confluence:
#
# - https://siana-systems.atlassian.net/wiki/spaces/HLAB/pages/1395327342/MP.CPU+using+the+Developer+task.py
#
# This python scripts uses Python Invoke to define project tasks:
#   - run: invoke --list
#
# The script is expected to be called from the ./meta-mpcam/mx directory and 
# it should be configured for the project in the TUNABLES section below.
#
# Requirements (in Path):
#    - Python 3.x
#    - git client
#
# Installing dependencies:
#    - Invoke: pip install invoke
#    
# Usage: see invoke => http://pyinvoke.org
#------------------------------------------------------------------------------
# The MIT License (MIT)
# 
# Copyright (c) 2020 Siana Systems
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#  
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#------------------------------------------------------------------------------

__author__ = "SIANA Systems"
__license__ = "MIT"
__version__ = "ALPHA"
__email__ = "support@siana-systems.com"

import sys, os, time
from invoke import task
from invoke import Collection
from shutil import copyfile, copy2, rmtree
from datetime import timedelta

#--->> TUNABLES <<-----------------------------------------

# CubeMX DT project directory (in <git>/meta-mpcam/mx/):
# note: this folder must contain: ./CA7/DeviceTree/mpcam/[kernel|tf-a|u-boot]
MX_PRJ = "mpcam"        # <git>/meta-mpcam/mx/mpcam
#MX_PRJ = "brk"        # <git>/meta-mpcam/mx/brk
#MX_PRJ = "test"        # <git>/meta-mpcam/mx/test

# relative path (from ~) of the target Developer Package root directory:
DEVPACK_ROOT = 'opt/OpenSTLinux/MPCam/Developer-Package/stm32mp1-openstlinux-5-4-dunfell-mp1-20-11-12'

# paths to source directories (relative to DEVPACK_ROOT):
# note: where tfa/uboot/linux sources are! not the parent directory containing the ST patches.
TFA_SRC_PATH = os.path.join(DEVPACK_ROOT,'sources/arm-ostl-linux-gnueabi/tf-a-stm32mp-2.2.r2-r0/tf-a-stm32mp-2.2.r2')
UBOOT_SRC_PATH = os.path.join(DEVPACK_ROOT,'sources/arm-ostl-linux-gnueabi/u-boot-stm32mp-2020.01.r2-r0/u-boot-stm32mp-2020.01.r2')
LINUX_SRC_PATH = os.path.join(DEVPACK_ROOT,'sources/arm-ostl-linux-gnueabi/linux-stm32mp-5.4.56-r0/linux-5.4.56')

#==========================================================

# path to this working dir (should be: <git>/meta-mpcam/mx):
root_path = os.path.realpath(os.path.join(os.path.dirname(__file__)))

# path to CubeMX device-tree root (contains kernel/tf-a/u-boot directories):
dt_mx_path = os.path.join(root_path,MX_PRJ,'CA7/DeviceTree/mpcam')

# paths to source directfories:
tfa_path = os.path.join(os.path.expanduser('~'),TFA_SRC_PATH)
uboot_path = os.path.join(os.path.expanduser('~'), UBOOT_SRC_PATH)
linux_path = os.path.join(os.path.expanduser('~'), LINUX_SRC_PATH)

#--->> Helper Functions <<---------------------------------

def check_gcc_environment(c):
    """
    checks if the current environment has been sourced for ST cross-toolchain
    """
    r = c.run("$CC --version", hide=False)
    if r.stdout.find('arm-ostl-linux-gnueabi-gcc') >= 0: 
        return True

    return False

def check_source_directories(verbose=False):
    """
    checks if the destination "sources" directory contains source of TF-A/U-Boot/Linux
    """
    is_ok = True
    if os.path.isdir( tfa_path ) == False:
        if verbose: print("Missing TF-A: {}".format(tfa_path))
        is_ok = False

    if os.path.isdir( uboot_path ) == False:
        if verbose: print("Missing U-Boot: {}".format(uboot_path))
        is_ok = False

    if os.path.isdir( linux_path ) == False:
        if verbose: print("Missing Linux: {}".format(linux_path))
        is_ok = False

    return is_ok

def print_elapsed_time(start, end, msg="Duration"):
    elapsed_time_secs = end - start
    print("{}: {}".format(msg, timedelta(seconds=round(elapsed_time_secs)))) 

#==========================================================
#  T A S K S 
#----------------------------------------------------------
    
@task
def version(c):
    print("\n** meta-mpcam **\n")

    print("WARNING: you MUST source your build environment!\n")

    print("Root: {}\n".format(root_path))

    print("checking cross-toolchain...")
    if check_gcc_environment(c) == False:
        print(">> WARNING: you MUST source your build environment!")


    if check_source_directories(True):
        print("* TF-A source:\n{}\n".format( tfa_path ))
        print("* U-Boot source:\n{}\n".format( uboot_path ))
        print("* Linux source:\n{}\n".format( linux_path ))
    else:
        print("> ERROR: One/more source directories not found!")

#--->> TF-A <<-----------------------------------
   
@task
def copy_tfa(c):
    """
    Copies the CubeMX TF-A device-tree into the source tree.
    """
    print(">> copying TF-A device-tree into the source tree...")
    mx_dir = os.path.join(dt_mx_path,'tf-a')
    src_dir = os.path.join(tfa_path, 'fdts')
    copy2(os.path.join(mx_dir,'stm32mp15-mx.dtsi'), os.path.join(src_dir))
    copy2(os.path.join(mx_dir,'stm32mp157c-mpcam-mx.dts'), os.path.join(src_dir))

@task
def patch_tfa(c):
    """
    Applies the ST patches to the TF-A source.
    """
    print(">> applying TF-A patches...")
    with c.cd(tfa_path):
        c.run("for p in `ls -1 ../*.patch`; do patch -p1 < $p; done")

@task
def build_tfa(c):
    """
    Builds the TF-A..
    """
    print(">> building TF-A...")
    build_start = time.time()
    with c.cd(tfa_path):
        c.run("make -f $PWD/../Makefile.sdk TFA_DEVICETREE=stm32mp157c-mpcam-mx TF_A_CONFIG=trusted ELF_DEBUG_ENABLE='1' all")
    print_elapsed_time(build_start, time.time(), ">> TF-A build")

@task
def clean_tfa(c):
    """
    Cleans the TF-A build
    """
    print(">> cleaning TF-A...")
    with c.cd(tfa_path):
        c.run("make -f $PWD/../Makefile.sdk clean")

#--->> U-Boot <<---------------------------------

@task
def copy_uboot(c):
    """
    Copies the CubeMX U-Boot device-tree into the source tree.
    """
    print(">> copying U-Boot device-tree into the source tree...")
    mx_dir = os.path.join(dt_mx_path,'u-boot')
    src_dir = os.path.join(uboot_path, 'arch/arm/dts')
    copy2(os.path.join(mx_dir,'stm32mp15-mx.dtsi'), os.path.join(src_dir))
    copy2(os.path.join(mx_dir,'stm32mp157c-mpcam-mx.dts'), os.path.join(src_dir))
    copy2(os.path.join(mx_dir,'stm32mp157c-mpcam-mx-u-boot.dtsi'), os.path.join(src_dir))
    print(">> WARNING: add to Makefile in <uboot-source>/arch/arm/dts => dtb-$(CONFIG_STM32MP15x) += stm32mp157c-mpcam-mx.dtb")

@task
def patch_uboot(c):
    """
    Applies the ST patches to the U-Boot source.
    """
    print(">> applying U-Boot patches...")
    with c.cd(uboot_path):
        c.run("for p in `ls -1 ../*.patch`; do patch -p1 < $p; done")

@task
def build_uboot(c):
    """
    Builds the U-Boot..
    """
    print(">> building U-Boot...")
    build_start = time.time()
    with c.cd(uboot_path):
        c.run("make -f $PWD/../Makefile.sdk all UBOOT_CONFIGS=stm32mp15_trusted_defconfig,trusted,u-boot.stm32 DEVICE_TREE=stm32mp157c-mpcam-mx")
    print_elapsed_time(build_start, time.time(), ">> U-Boot build")

@task
def clean_uboot(c):
    """
    Cleans the U-Boot build
    """
    print(">> cleaning U-Boot...")
    with c.cd(uboot_path):
        c.run("make -f $PWD/../Makefile.sdk clean")

#--->> Linux <<----------------------------------

@task
def copy_linux(c):
    """
    Copies the CubeMX Linux device-tree into the source tree.
    """
    print(">> copying Linux device-tree into the source tree...")
    mx_dir = os.path.join(dt_mx_path,'kernel')
    src_dir = os.path.join(linux_path, 'arch/arm/boot/dts')
    copy2(os.path.join(mx_dir,'stm32mp157c-mpcam-mx.dts'), os.path.join(src_dir))
    print(">> WARNING: add to Makefile in <linux-source>/arch/arm/boot/dts => dtb-$(CONFIG_ARCH_STM32) += stm32mp157c-mpcam-mx.dtb")

@task
def patch_linux(c):
    """
    Applies the ST patches to the Linux source.
    """
    print(">> applying linux patches...")
    with c.cd(linux_path):
        c.run("for p in `ls -1 ../*.patch`; do patch -p1 < $p; done")

@task
def config_linux(c):
    """
    Configures the Linux source.
    """
    print(">> checks if the Linux build directory exists...")
    build_dir = os.path.join(linux_path,'..','build')
    if not os.path.exists(build_dir):
        print(">> creating the Linux build directory...")
        os.makedirs(build_dir)

    print(">> configure the linux source...")
    with c.cd(linux_path):
        c.run('make ARCH=arm O="$PWD/../build" multi_v7_defconfig fragment*.config')
        c.run(r"""for f in `ls -1 ../fragment*.config`; do scripts/kconfig/merge_config.sh -m -r -O $PWD/../build $PWD/../build/.config $f; done""")
        c.run("yes '' | make ARCH=arm oldconfig O=\"$PWD/../build\"")

@task
def build_linux(c, target='all'):
    """
    Builds the Linux..
    """    
    build_dir = os.path.join(linux_path,"..","build")
    build_start = time.time()
    kernel_start = kernel_end = 0
    modules_start = modules_end = 0
    artifacts_start = artifacts_end = 0
    
    if target == 'linux' or target == 'all':
        print(">> building Linux kernel images and dtbs...")
        kernel_start = time.time()
        with c.cd(build_dir):
            c.run('make ARCH=arm uImage vmlinux dtbs LOADADDR=0xC2000040')
        kernel_end = time.time()
        print_elapsed_time(kernel_start, kernel_end, ">> Kernel build")
    
    if target == 'modules' or target == 'all':
        print(">> building Linux modules...")
        modules_start = time.time()
        with c.cd(build_dir):
            c.run('make ARCH=arm modules')
        modules_end = time.time()
        print_elapsed_time(modules_start, modules_end, ">> Modules build")

    if target == 'artifacts' or target == 'all':
        print(">> generating Linux output build artifacts...")
        artifacts_start = time.time()
        with c.cd(build_dir):
            c.run('make ARCH=arm INSTALL_MOD_PATH="$PWD/../build/install_artifact" modules_install')
            c.run('mkdir -p $PWD/../build/install_artifact/boot/')
            c.run('cp $PWD/../build/arch/arm/boot/uImage $PWD/../build/install_artifact/boot/')
            c.run('cp $PWD/../build/arch/arm/boot/dts/st*.dtb $PWD/../build/install_artifact/boot/')
        artifacts_end = time.time()

    # print build summary...
    print_elapsed_time(build_start, time.time(),       ">> FULL Linux build")
    print_elapsed_time(kernel_start, kernel_end,       "\tKernel build")
    print_elapsed_time(modules_start, modules_end,     "\tModules build")
    print_elapsed_time(artifacts_start, artifacts_end, "\tArtifacts build")        

@task
def build_linux_dt(c):
    """
    Builds the Linux Device-Tree
    """
    build_dir = os.path.join(linux_path,"..","build")
    print(">> building Linux Device-Tree...")
    with c.cd(build_dir):
        c.run('make ARCH=arm stm32mp157c-mpcam-mx.dtb LOADADDR=0xC2000040 O="$PWD/../build"')
        c.run('cp $PWD/../build/arch/arm/boot/dts/stm32mp157c-mpcam-mx.dtb $PWD/../build/install_artifact/boot/') 

@task
def clean_linux(c):
    """
    Cleans the U-Boot build
    """
    build_dir = os.path.join(linux_path,"..","build")
    print(">> cleaning Linux build...")
    if os.path.exists(build_dir):
        rmtree(build_dir)

#--->> ALL <<------------------------------------

@task(copy_tfa, copy_linux, copy_uboot)
def copy_all(c):
    """
    Copies the CubeMX device-tree from TF-A/U-Boot/Linux into the source tree.
    """

@task(build_tfa, build_uboot, build_linux)
def build_all(c):
    """
    Builds the TF-A / U-Boot / Linux.
    """
    print(">> Images built!")

#--->> SDCard Utilities <<-----------------------

@task
def update_sdX(c, sdx, target='all'):
    """
    Update an SDCard partition: --target=[tfa|uboot|linux|dt|modules|all(default)]
    """

    print(">> updating {}...".format(sdx))

    if target == "tfa" or target == 'all':
        print("\n>> updating {}1 with TF-A...".format(sdx))
        tfa_image = os.path.join(tfa_path,'..','build/trusted/tf-a-stm32mp157c-mpcam-mx-trusted.stm32')
        print("dd if={0} of=/dev/{1}1 bs=1M conv=fdatasync".format(tfa_image,sdx))
        c.run("sudo dd if={0} of=/dev/{1}1 bs=1M conv=fdatasync".format(tfa_image,sdx))  
        print(">> updating {}2 with TF-A...".format(sdx))
        print("dd if={0} of=/dev/{1}2 bs=1M conv=fdatasync".format(tfa_image,sdx))
        c.run("sudo dd if={0} of=/dev/{1}2 bs=1M conv=fdatasync".format(tfa_image,sdx))        

    if target == "uboot" or target == 'all':
        print("\n>> updating {}3 with U-Boot...".format(sdx))
        uboot_image = os.path.join(uboot_path,'..','build-trusted/u-boot-stm32mp157c-mpcam-mx-trusted.stm32')
        print("dd if={0} of=/dev/{1}3 bs=1M conv=fdatasync".format(uboot_image,sdx))
        c.run("sudo dd if={0} of=/dev/{1}3 bs=1M conv=fdatasync".format(uboot_image,sdx))

    if target == "linux" or target == 'all': 
        print("\n>> updating {}/bootfs with uImage...".format(sdx))
        uImage_image = os.path.join(linux_path,"..",'build/install_artifact/boot/uImage')
        print("sudo cp {} /media/$USER/bootfs/".format(uImage_image))
        c.run("sudo cp {} /media/$USER/bootfs/".format(uImage_image))

    if target == "dt" or target == 'all':
        print("\n>> updating {}/bootfs with device-tree...".format(sdx))
        dtb_image = os.path.join(linux_path,"..",'build/install_artifact/boot/stm32mp157c-mpcam-mx.dtb')
        print("sudo cp {} /media/$USER/bootfs/".format(dtb_image))
        c.run("sudo cp {} /media/$USER/bootfs/".format(dtb_image))

    if target == "modules" or target == 'all':
        print("\n>> updating {}/rootfs with kernel modules...".format(sdx))
        modules_src = os.path.join(linux_path,"..",'build/install_artifact/lib/modules/5.4.56')
        modules_dst = "/media/$USER/rootfs/lib/modules/5.4.56"
        
        # clean sdcard modules.*
        if os.path.exists(os.path.join(modules_dst,'modules.alias')):
            c.run("sudo rm {}/modules.*".format(modules_dst))

        # clean sdcard modules/kernel/
        if os.path.exists(os.path.join(modules_dst,'kernel')):
            c.run("sudo rm -r {}/kernel".format(modules_dst))

        # copy modules.*
        print("sudo cp {}/modules.* {}".format(modules_src, modules_dst))
        c.run("sudo cp {}/modules.* {}".format(modules_src, modules_dst))

        # copy /kernel
        print("sudo cp -r {}/kernel {}/kernel".format(modules_src, modules_dst))
        c.run("sudo cp -r {}/kernel {}/kernel".format(modules_src, modules_dst))

@task
def save_assets(c, to='pwd'):
    """
    Copies the build outputs/assets to a directory.
    """

    tfa_image = os.path.join(tfa_path,'..','build/trusted/tf-a-stm32mp157c-mpcam-mx-trusted.stm32')
    uboot_image = os.path.join(uboot_path,'..','build-trusted/u-boot-stm32mp157c-mpcam-mx-trusted.stm32')
    uImage_image = os.path.join(linux_path,"..",'build/install_artifact/boot/uImage')
    dtb_image = os.path.join(linux_path,"..",'build/install_artifact/boot/stm32mp157c-mpcam-mx.dtb')

    if to == 'pwd':
        dst_path = os.path.join(root_path,'assets')
    else:
        dst_path = to

    if not os.path.exists(dst_path):
        print(">> creating assets directory...")
        os.makedirs(dst_path)

    copy2(tfa_image, dst_path)
    copy2(uboot_image, dst_path)
    copy2(uImage_image, dst_path)
    copy2(dtb_image, dst_path)


