#!/bin/bash
# Default values
PATH_CACHE=/cache
PATH_REPO=/repo
PATH_BUILD=$PATH_REPO/build
PATH_SSH=/home/builder/.ssh

# Use arguments intead of default values, if provided
while getopts "b:s:c:" OPTION; do
    case $OPTION in
        b) PATH_BUILD=${OPTARG};;
        s) PATH_SSH=${OPTARG};;
        c) PATH_CACHE=${OPTARG};;
        *) exit 1;;
    esac
done

# Add ssh keys, if any
if [ -d $PATH_SSH ]; then
	ssh-keyscan bitbucket.org > $PATH_SSH/known_hosts
fi

# Set OE environment
source layers/openembedded-core/oe-init-build-env $PATH_BUILD
