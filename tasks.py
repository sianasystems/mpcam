#!/usr/bin/env python
##
# @file       tasks.py
# @author     SIANA Systems
# @date       12/2020
# @copyright  The MIT License (MIT)
# @brief      Python Invoke tasks script for OE projects. 
#
# This python scripts uses Python Invoke to run project tasks:
#   - run: invoke --list (linux)
#   - run: python -m invoke --list (windows)
#
# The script is expected to be called from the OE project root and 
# it is configured for the project in the TUNABLES section.
#
# Requirements (in Path):
#    - Python 3.x
#    - git client
#
# Installing dependencies:
#    - Invoke: pip install invoke
#    
# Usage: see invoke => http://pyinvoke.org
#------------------------------------------------------------------------------
# The MIT License (MIT)
# 
# Copyright (c) 2020 Siana Systems
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#  
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#------------------------------------------------------------------------------
__author__ = "SIANA Systems"
__license__ = "MIT"
__version__ = "ALPHA 4"
__email__ = "sebastian@siana-systems.com"

import sys, os
from scripts.bitbaker import Constants, Bitbaker

from invoke import task

#------------------------------------------------------------------------------
## PROJECT TUNABLES
#------------------------------------------------------------------------------
# Project client name
CLIENT = "SIANA"

# OE Machine to be used in builds
MACHINE = "mpcam"

# OE Distro to be used in builds
DISTRO = "mpcam"

# Default OE Image to be used in builds
IMAGE = "mpcam-image-demo"

# Path to the OE file where the version will be obtained (RELEASE parameter)
# WARNING: Path must be specified from PWD of this script
PATH_DISTRO_VERSION = f"layers/meta-{DISTRO}/conf/distro/include/{DISTRO}.inc"

# Configure capabilities to include
CUBEMX = True
MENDER = False
# Configure layers
LAYERS = Bitbaker.generateLayers(with_cubemx=CUBEMX, with_mender=MENDER)
# Additional layers to be used by the Project
LAYERS += f"{Constants.PATH_LAYER}/meta-st-stm32mpu-ai \
    {Constants.PATH_LAYER}/meta-python2 \
    {Constants.PATH_LAYER}/meta-aws \
    {Constants.PATH_LAYER}/meta-mpcam \
    {Constants.PATH_LAYER}/meta-swupdate"

# Shared docker volume for OE cache.
HOST_PATH_CACHE = "/data/bb.cache"

# Path for SSH keys
HOST_PATH_SSH = "/home/$USER/.ssh"

#------------------------------------------------------------------------------
## Helpers
#------------------------------------------------------------------------------
PATH_PWD = os.path.realpath(os.path.join(os.path.dirname(__file__)))
DOCKER_MOUNT = f"-v {HOST_PATH_CACHE}:{Constants.PATH_CACHE}"
DOCKER_MOUNT += f" -v {PATH_PWD}:{Constants.PATH_REPO}"
DOCKER_MOUNT += f" -v {HOST_PATH_SSH}:{Constants.PATH_SSH}"


#-----------------------------------------------------------------------------
## Tasks
#------------------------------------------------------------------------------
@task
def init(c):
    """
    Adds layers dependencies for core OE and STM32MP1 as git submodules.
    Useful when starting a project from scratch. Verify that PATH_SOURCES
    points to the proper OSTL Release.

    :param c: Task context.
    """
    c.run(Bitbaker.dockerRunCmd(DOCKER_MOUNT, Bitbaker.getSourcesCmd(with_cubemx=CUBEMX, with_mender=MENDER)))

@task
def setup(c, machine=MACHINE, distro=DISTRO):
    """
    Setup OpenEmbedded Environment (OE), custom layers and configurations.

    :param c: Task context.
    :param str machine: OE machine.
    :param str distro: OE Distro.
    """
    cmds = f"{Bitbaker.configureLayersCmd(layers=LAYERS)} && {Bitbaker.configureBBCmd(distro=distro, machine=machine, with_mender=MENDER, secureRoot=True)}"
    c.run(Bitbaker.dockerRunCmd(DOCKER_MOUNT, f"rm -rf {Constants.PATH_BUILD}/conf"))
    c.run(Bitbaker.runBBCmd(DOCKER_MOUNT, cmds))


@task
def build(c, recipe=IMAGE):
    """
    Runs bitbake command against a recipe/image.

    :param c: Task context.
    :param str recipe: OE recipe to be built.
    """
    c.run(Bitbaker.runBBCmd(DOCKER_MOUNT, f"bitbake {recipe}"))

@task
def manual(c, privileged=False):
    """
    Opens the docker container setup to work with bitbake.
    If it's going to be used for bitbake, remember to:
    1. Make sure that setup task was run.
    2. Run `source env.sh` to initialize manually the OE env.

    :param c: Task context.
    """
    c.run(Bitbaker.dockerIt(DOCKER_MOUNT, privileged=privileged), pty=True)

@task
def release(c, sdcard=True, emmc=False, sdk=False, image=IMAGE):
    """
    Generates a release of the selected deploy.

    :param c: Task context.
    :param bool sdcard: If true, generates SD Card release.
    :param bool emmc: If true, generates eMMC release.
    :param bool sdk: If true, generates SDK release.
    """
    version = Bitbaker.getVersion(PATH_DISTRO_VERSION)

    machine = MACHINE
    if sdcard or emmc:
        build(c, recipe=image)
    if sdcard:
        c.run(Bitbaker.dockerRunCmd(DOCKER_MOUNT, Bitbaker.makeSDCard(CLIENT, None, image, machine, version)))
    if emmc:
        c.run(Bitbaker.dockerRunCmd(DOCKER_MOUNT, Bitbaker.makeEMMC(CLIENT, None, image, machine, version)))
    if sdk:
        c.run(Bitbaker.runBBCmd(DOCKER_MOUNT, f"bitbake -c populate_sdk {image}"))
        c.run(Bitbaker.dockerRunCmd(DOCKER_MOUNT, Bitbaker.makeSDK(CLIENT, None, image, machine, version)))
    
@task
def update(c, update):
    """
    Generated updates packages for SWUpdate
    :param c: Task context.
    :param bool rootfs: If true, generates rootfs update package.
    :param bool appfs: If true, generates appfs update package.
    """
    build(c, recipe=update)
    c.run(Bitbaker.dockerRunCmd(DOCKER_MOUNT, Bitbaker.getArtifact(update)))
    build(c, recipe=update+"-update")
    
    # get SWU files
    c.run(Bitbaker.dockerRunCmd(DOCKER_MOUNT, Bitbaker.getSWU(update, MACHINE)))

@task
def findPackage(c, package):
    c.run(Bitbaker.dockerRunCmd(DOCKER_MOUNT, Bitbaker.findPackage(package)))

@task
def packageInfo(c, path, keep=False):
    c.run(Bitbaker.dockerRunCmd(DOCKER_MOUNT, Bitbaker.packageInfo(path, keep)))

