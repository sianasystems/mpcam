# MPCam BSP

## Prerequisites
- At least 150 GB of EXT3/4 disk space.
- Any Linux distribution capable of running Docker.
- (Optional) WSL2 on Windows could be used, with WSL2 based engine for docker.

## Requisites
- [Docker](https://www.docker.com/) 20.10+.
- [Python](https://www.python.org/downloads/) 3.8+.
    - [pyinvoke](http://www.pyinvoke.org/installing.html) 1.3+.
- [Git](https://git-scm.com/downloads) 2.25+.

# Setup
1. Customize `tasks.py` file on project's root in project tunables section.
2. Configure proper paths for:
    - `HOST_PATH_CACHE` path for build cache and downloads.
    - `HOST_PATH_SSH` path to SSH credentials, if needed (private repository access).
3. Make sure to clone the repository with all it's submodules.
    ```
    $ git clone --recurse-submodules git clone git@bitbucket.org:sianasystems/mpcam.git 
    ```
4. Setup the project. This needs to be run only the first time and after any changes to `tasks.py`. This will setup `build` folder on project's root where configurations/builds will be stored.
    ```
    $ inv setup
    ```

# Instructions
1. To generate the default image, execute release task. A zip package will be generated on project’s root folder.
    ```
    $ inv release
    ```
2. To build a specific package (in this example, `mpcam-http` package), execute build task.
    ```
    $ inv build -r mpcam-http
    ```
3. To access the docker container, execute manual task. Once loaded, activate the environrment (`env.sh`) to start working with `bitbake`.
    ```
    $ inv manual
    builder@72e7aab0670a:/repo$ source env.sh

    ### Shell environment set up for builds. ###

    You can now run 'bitbake <target>'

    Common targets are:
        core-image-minimal
        core-image-sato
        meta-toolchain
        meta-ide-support

    You can also run generated qemu images with a command like 'runqemu qemux86'.

    Other commonly useful commands are:
    - 'devtool' and 'recipetool' handle common recipe tasks
    - 'bitbake-layers' handles common layer tasks
    - 'oe-pkgdata-util' handles common target package tasks
    builder@72e7aab0670a:/repo/build$
    ```
4. To list all available tasks, execute `list`.
    ```
    inv --list
    ```
